#include "Arduino.h"

void setup() {
    // initialize digital pin LED_BUILTIN as an output.
    pinMode(PIN_PA6, OUTPUT);
    pinMode(PIN_PA7, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
    digitalWrite(PIN_PA7, HIGH);  // turn the LED on (HIGH is the voltage level)
    digitalWrite(PIN_PA6, HIGH);  // turn the LED on (HIGH is the voltage level)
    delay(1000);                      // wait for a second
    digitalWrite(PIN_PA7, LOW);   // turn the LED off by making the voltage LOW
    digitalWrite(PIN_PA6, LOW);   // turn the LED off by making the voltage LOW
    delay(1000);                      // wait for a second
}
