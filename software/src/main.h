#ifndef SOFTWARE_MAIN_H
#define SOFTWARE_MAIN_H

#include "Arduino.h"

uint8_t write_flash_pages(int length);
uint8_t write_eeprom_chunk(unsigned int start, unsigned int length);
void avrisp();

#endif //SOFTWARE_MAIN_H
