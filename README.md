# Integrated Serial Adapter

Based on : https://hackaday.io/project/190437-integrated-serial-adapter-isa

## BOM

| QUANTITY 	| COMPONENT NAME                                           	|
|----------	|----------------------------------------------------------	|
| 1        	| ATtiny814 Microcontroller, SOIC-14 package               	|
| 1        	| CH330N or (CH340N) USB-to-Serial Converter, SOIC-8 package 	|
| 1        	| USB Type-C Connector 16-pin, USB 2.0 specification (6 pin)      	|
| 1        	| RT9193-33GB 3.3V LDO Regulator, SOT-23-5 package  (9013)       	|
| 3        	| MSS22D18 DPDT SMD switch                                 	|
| 1        	| 1N5819 SMD Schottky Diode, SOD-123 package  (323)               	|
| 6        	| LED SMD, 0805                               (x)             	|
| 4        	| 100nF SMD Ceramic Capacitor, 0805                        	|
| 2        	| 1uF X5R or X7R SMD Ceramic Capacitor, 0805               	|
| 1        	| 10uF SMD Ceramic Capacitor, 0805                         	|
| 1        	| 470R SMD Resistor, 0805                                  	|
| 4        	| 1K SMD Resistor, 0805                                    	|
| 3        	| 5.1K SMD Resistor, 0805                                  	|
